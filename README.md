# ics-ans-ccce

Ansible playbook to install CCCE.

## Usage

This playbook will set up a host in order to run the CCCE deployment tool. This will further set up the necessary
infrastructure in the appropriate AWX instance that is needed for the deployment tool to run correctly. There is
a small amount of setup work that needs to be in place first, however:
* You must add a machine credential to the appropriate AWX instance to allow it to log into the hosts that you
  would like it to deploy to
* You must create a technical user on the AWX instance that is used by the deployment tool to trigger AWX jobs
* You must create a token for the technical user to allow it access to the AWX instance
* That token should be stored in `ccce_awx_token` (it should, of course, be vaulted.)

Once this is done, you should be able to run this playbook on your desired host, and have a working CCCE
deployment tool.

## License

BSD 2-clause

