import os
import json
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ccce')


def test_containers(host):
    with host.sudo():
        database = host.docker("ccce-database")
        backend = host.docker("ce-deploy-backend")
        frontend = host.docker("ce-deploy-ui")
        assert database.is_running
        assert backend.is_running
        assert frontend.is_running


def test_backend(host):
    cmd = host.run("curl --insecure --fail https://ccce-default/api/spec")
    assert cmd.rc == 0
    assert json.loads(cmd.stdout)["info"]["title"] == "CE deploy & monitor API"


def test_frontend(host):
    cmd = host.run("curl --insecure --fail https://ccce-default/")
    assert cmd.rc == 0
    assert '<meta name="description" content="Web site created using create-react-app"/>' in cmd.stdout
